//
//  main.m
//  BliztalkTest
//
//  Created by Rafal Augustyniak on 20.06.2014.
//  Copyright (c) 2014 Rafal Augustyniak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RAGrandChildObject.h"

int main(int argc, const char * argv[])
{
  @autoreleasepool {
    
    id object = [RAGrandChildObject sharedInstance];
    
    NSLog(@"%@", NSStringFromClass([object class]));
  }
  
  return 0;
}

