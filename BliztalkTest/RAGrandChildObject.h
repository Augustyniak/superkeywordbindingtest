//
//  RAGrandChildObject.h
//  BliztalkTest
//
//  Created by Rafal Augustyniak on 20.06.2014.
//  Copyright (c) 2014 Rafal Augustyniak. All rights reserved.
//

#import "RAChildObject.h"

@interface RAGrandChildObject : RAChildObject

@end
