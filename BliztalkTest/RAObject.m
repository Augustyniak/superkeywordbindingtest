//
//  RAObject.m
//  BlitztalkTest
//
//  Created by Rafal Augustyniak on 20.06.2014.
//  Copyright (c) 2014 Rafal Augustyniak. All rights reserved.
//

#import "RAObject.h"

@implementation RAObject

+ (instancetype)alloc
{
  return [self allocWithZone:nil];
}

+ (instancetype)funnyAlloc
{
  return [super allocWithZone:nil];
}

+ (instancetype)allocWithZone:(NSZone *)zone
{
  NSLog(@"You can't alloc another instance of an object.");
  return nil;
}

+ (instancetype)new
{
  return [self alloc];
}

- (instancetype)copyWithZone:(NSZone *)zone
{
  return self;
}

- (instancetype)mutableCopyWithZone:(NSZone *)zone
{
  return [self copyWithZone:zone];
}

+ (instancetype)sharedInstance
{
  static id instance = nil;
  
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    instance = [[[self class] funnyAlloc] init];
  });
  
  return instance;
}

@end
