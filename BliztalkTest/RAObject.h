//
//  RAObject.h
//  BliztalkTest
//
//  Created by Rafal Augustyniak on 20.06.2014.
//  Copyright (c) 2014 Rafal Augustyniak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RAObject : NSObject

+ (instancetype)sharedInstance;

@end
